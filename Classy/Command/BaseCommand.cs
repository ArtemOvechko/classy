using ClassyLib;

namespace Classy.Command;

public abstract class BaseCommand : ICommand
{
    public virtual async Task Execute(ClassyApp classyApp)
    {
        await classyApp.SettingsManager.Init();
    }
}