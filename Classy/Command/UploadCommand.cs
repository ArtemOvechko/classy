using ClassyLib;
using CommandLine;

namespace Classy.Command;

[Verb("upload", HelpText = "Upload data set to s3")]
public class UploadCommand : BaseCommand
{
    [Option('p', "path", Required = true, HelpText = "Path to the processed data set file")]
    public string? Path { get; set; }
    
    [Option('b', "s3bucket", Required = true, HelpText = "S3 bucket name")]
    public string? BucketName { get; set; }
    
    [Option('d', "s3directory", Required = true, HelpText = "Output file name")]
    public string? BucketDirectoryName { get; set; }

    public override async Task Execute(ClassyApp classyApp)
    {
        try
        {
            await base.Execute(classyApp);
            await classyApp.S3Storage!.Upload(Path!, BucketName!, BucketDirectoryName!);
            classyApp.Exit(0);
        }
        catch (Exception ex)
        {
            Console.WriteLine();
            System.Console.WriteLine("Upload failed. Exception:");
            System.Console.WriteLine(ex.Message);
            classyApp.Exit(1);
        }
    }
}