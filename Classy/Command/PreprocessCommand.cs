using System.Globalization;
using ClassyLib;
using CommandLine;

namespace Classy.Command;

[Verb("prepare", HelpText = "Prepare data set")]
public class PreprocessCommand : BaseCommand
{
    [Option('p', "path", Required = true, HelpText = "Path to the data set file")]
    public string? Path { get; set; }
    
    [Option('c', "classpath", Required = true, HelpText = "Path to the data classes file")]
    public string? ClassPath { get; set; }
    
    [Option('o', "output", Required = true, HelpText = "Output file name")]
    public string? OutputFile { get; set; }
    
    [Option('r', "percent", Required = false, HelpText = "Data chunk size to preprocess")]
    public string? Percentage { get; set; }
    
    public override async Task Execute(ClassyApp classyApp)
    {
        try
        {
            //await base.Execute();
            await classyApp.Preprocessor.PreprocessAsync(
                Path!,
                ClassPath!,
                OutputFile!, 
                Percentage != null ? float.Parse(Percentage, CultureInfo.InvariantCulture) : 1f);
            classyApp.Exit(0);
        }
        catch (Exception ex)
        {
            Console.WriteLine();
            System.Console.WriteLine("Preprocessing failed. Exception:");
            System.Console.WriteLine(ex.Message);
            classyApp.Exit(1);
        }
    }
}