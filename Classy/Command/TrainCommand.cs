using ClassyLib;
using CommandLine;

namespace Classy.Command;

[Verb("train", HelpText = "Train SageMaker model")]
public class TrainCommand : BaseCommand
{
    [Option('t', "s3-training-path", Required = true, HelpText = "S3 Path to the processed data set training file")]
    public string? S3TrainingPath { get; set; }
    
    [Option('v', "s3-validation-path", Required = true, HelpText = "S3 Path to the processed data set validation file")]
    public string? S3ValidationPath { get; set; }
    
    [Option('o', "s3-output-path", Required = true, HelpText = "S3 Path to the output file")]
    public string? S3OutputPath { get; set; }

    public override async Task Execute(ClassyApp classyApp)
    {
        try
        {
            await base.Execute(classyApp);
            await classyApp.SageMakerManager!.PrepareModelAsync(
                S3TrainingPath!, 
                S3ValidationPath!, 
                S3OutputPath!,
                classyApp.SettingsManager!.Settings!.IAMRoleNameForSetup!);
            classyApp.Exit(0);
        }
        catch (Exception ex)
        {
            Console.WriteLine();
            System.Console.WriteLine("Training failed. Exception:");
            System.Console.WriteLine(ex.Message);
            classyApp.Exit(1);
        }
    }
}