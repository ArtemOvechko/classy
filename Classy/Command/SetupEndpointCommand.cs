using ClassyLib;
using CommandLine;

namespace Classy.Command;

[Verb("endpoint", HelpText = "Setup SageMaker endpoint")]
public class SetupEndpointCommand : BaseCommand
{
    [Option('m', "model", Required = true, HelpText = "SageMaker model name")]
    public string? S3TrainingPath { get; set; }
    
    public override async Task Execute(ClassyApp classyApp)
    {
        try
        {
            await base.Execute(classyApp);
            await classyApp.SageMakerManager!.SetupEndpointAsync(S3TrainingPath!);
            classyApp.Exit(0);
        }
        catch (Exception ex)
        {
            Console.WriteLine();
            System.Console.WriteLine("Endpoint setup failed. Exception:");
            System.Console.WriteLine(ex.Message);
            classyApp.Exit(1);
        }
    }
}