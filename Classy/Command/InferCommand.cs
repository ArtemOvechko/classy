using ClassyLib;
using CommandLine;

namespace Classy.Command;

[Verb("whatis", HelpText = "Call SageMaker inference endpoint")]
public class InferCommand : BaseCommand
{
    [Value(0, Required = true, HelpText = "Input string to classify")]
    public string? Input { get; set; }

    public override async Task Execute(ClassyApp classyApp)
    {
        try
        {
            await base.Execute(classyApp);
            await classyApp.SageMakerManager!.MakeInferenceAsync(
                Input!, classyApp.SettingsManager!.Settings!.InferenceEndpointName!);
            classyApp.Exit(0);
        }
        catch (Exception ex)
        {
            Console.WriteLine();
            System.Console.WriteLine("Inference failed. Exception:");
            System.Console.WriteLine(ex.Message);
            classyApp.Exit(1);
        }
    }
}