using ClassyLib;
using ClassyLib.Settings;
using CommandLine;

namespace Classy.Command;

[Verb("configure", HelpText = "Configure local classy settings in the directory")]
public class ConfigureCommand : ICommand
{

    [Option('e', "endpoint", Required = false, HelpText = "SageMaker endpoint name to call")]
    public string? EndpointName { get; set; }
    
    [Option('i', "iamrole", Required = false, HelpText = "AWS IAM Role to use when setting up things")]
    public string? IAMRoleName { get; set; }
    
    public async Task Execute(ClassyApp classyApp)
    {
        try
        {
            await classyApp.SettingsManager!.Configure(
                new Settings(EndpointName, IAMRoleName));
            classyApp.Exit(0);
        }
        catch (Exception ex)
        {
            Console.WriteLine();
            System.Console.WriteLine("Configuration failed. Exception:");
            System.Console.WriteLine(ex.Message);
            classyApp.Exit(1);
        }
    }
}