using ClassyLib;

namespace Classy.Command;

public interface ICommand
{
    Task Execute(ClassyApp classyApp);
}