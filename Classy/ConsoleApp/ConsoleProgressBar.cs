using System.Text;
using ClassyLib;

namespace Classy.ConsoleApp;

public class ConsoleProgressBar : IProgressBar
{
    private readonly StringBuilder _stringBuilder;
    private const short AwaiterCountMax = 15;
    private const short AwaiterIntervalMilliseconds = 50;
    private const short AwaiterIntervalMillisecondsFancy = 200;
    private short _awaiterCount = 0;
    private CancellationTokenSource _cts = new CancellationTokenSource();
    
    public ConsoleProgressBar()
    {
        Console.CursorVisible = false;
        _stringBuilder = new StringBuilder();
    }

    public void HandleExit()
    {
        Console.CursorVisible = true;
    }

    public void StartAwaiter(bool fancy = false)
    {
        _cts = new CancellationTokenSource();

        async void Action()
        {
            while (true)
            {
                if (_cts.IsCancellationRequested)
                {
                    return;
                }
                _stringBuilder.Clear();
                if (fancy)
                {
                    if (_awaiterCount % 4 == 0)
                    {
                        _stringBuilder.Append("(•_•)");
                    }

                    if (_awaiterCount % 4 == 1)
                    {
                        _stringBuilder.Append("( -_-)>⌐■-■");
                    }

                    if (_awaiterCount % 4 == 2 || _awaiterCount % 4 == 3)
                    {
                        _stringBuilder.Append("(⌐■_■)");
                    }

                    _stringBuilder.Append("       ");
                }
                else
                {
                    for (var i = 0; i < AwaiterCountMax; i++)
                    {
                        _stringBuilder.Append(i < _awaiterCount ? '.' : ' ');
                    }
                }

                Console.SetCursorPosition(0, Console.CursorTop);
                Console.Write(_stringBuilder.ToString());
                await Task.Delay(TimeSpan.FromMilliseconds(fancy
                    ? AwaiterIntervalMillisecondsFancy
                    : AwaiterIntervalMilliseconds));
                if (_awaiterCount++ >= AwaiterCountMax)
                {
                    _awaiterCount = 0;
                }
            }
        }

        new Task(Action, _cts.Token).Start();
    }

    public void Stop()
    {
        if (_cts is not { IsCancellationRequested: false })
        {
            _cts.Cancel();
        }
        Console.WriteLine();
    }
    
    /// <summary>
    /// Write progress bar
    /// </summary>
    /// <param name="value">Must be greater than zero.</param>
    /// <exception cref="ArgumentException"></exception>
    public void WritePercentComplete(short value)
    {
        if (value is < 0)
        {
            throw new ArgumentException("value must be greater than 0", nameof(value));
        }

        _stringBuilder.Clear();
        _stringBuilder.Append('[');
        for (var i = 0; i <= 100; i+=2)
        {
            if (i < value)
            {
                _stringBuilder.Append('=');
            }

            if (i == value)
            {
                _stringBuilder.Append(value < 100 ? '>' : '=');
            }

            if (i > value)
            {
                _stringBuilder.Append(value == i - 1 ? '>' : ' ');
            }
        }
        _stringBuilder.Append(']');
        Console.SetCursorPosition(0, Console.CursorTop);
        Console.Write(_stringBuilder.ToString());
    }
}