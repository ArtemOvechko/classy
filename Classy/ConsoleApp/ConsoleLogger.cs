using ClassyLib.IO;

namespace Classy.ConsoleApp;

public class ConsoleLogger : ILogger
{
    public void Info(string data)
    {
        Console.WriteLine(data);
    }
}