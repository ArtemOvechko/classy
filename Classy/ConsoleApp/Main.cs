using Classy.Command;
using Classy.ConsoleApp;
using ClassyLib;
using Microsoft.Extensions.DependencyInjection;
using CommandLine;


var services = new ServiceCollection()
    .AddClassy<ConsoleProgressBar, ConsoleLogger>();
var serviceProvider = services.BuildServiceProvider();

var app = serviceProvider.GetService<ClassyApp>()!;

Parser.Default.ParseArguments<
        PreprocessCommand, 
        UploadCommand,
        TrainCommand,
        SetupEndpointCommand,
        ConfigureCommand,
        InferCommand>(args)
    .WithParsed<PreprocessCommand>(async x => await x.Execute(app))
    .WithParsed<UploadCommand>(async x => await x.Execute(app))
    .WithParsed<TrainCommand>(async x => await x.Execute(app))
    .WithParsed<SetupEndpointCommand>(async x => await x.Execute(app))
    .WithParsed<ConfigureCommand>(async x => await x.Execute(app))
    .WithParsed<InferCommand>(async x => await x.Execute(app));
while (true)
{

}