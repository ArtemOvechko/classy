using ClassyLib.IO;

namespace ClassyLib;

/// <summary>
/// Responsible for data class mapping
/// </summary>
public class DataClassMapper
{
    private readonly CsvFileReader _csvFileReader;
    private readonly IProgressBar _progressBar;
    private readonly ILogger _logger;
    
    public DataClassMapper(
        CsvFileReader csvFileReader, 
        IProgressBar progressBar,
        ILogger logger)
    {
        _csvFileReader = csvFileReader;
        _progressBar = progressBar;
        _logger = logger;
    }

    /// <summary>
    /// Update class indices with actual names in all rows
    /// </summary>
    /// <param name="rows">Rows to update</param>
    /// <param name="classesFilePath">A txt file with one class at each line</param>
    public async Task<List<Row<string>>> MapTo(List<Row<string>> rows, string classesFilePath)
    {
        var classes = await LoadClasses(classesFilePath);
        var i = 0f;
        _logger.Info("Mapping classes to dataset...");
        var newRows = new List<Row<string>>(rows.Capacity);
        foreach (var row in rows)
        {
            if (int.TryParse(row[0], out var index))
            {
                row[0] = classes[index];
                newRows.Add(row);
            }
            _progressBar.WritePercentComplete((short)(100*i++/rows.Count));
        }
        _progressBar.WritePercentComplete(100);
        _progressBar.Stop();
        _logger.Info("Done.");
        return newRows;
    }

    private async Task<Dictionary<int, string>> LoadClasses(string filePath)
    {
        Dictionary<int, string> classMap = new Dictionary<int, string>();
        var classes = await _csvFileReader.ReadAsync(filePath, ',');
        var i = 1;
        foreach (var row in classes)
        {
            classMap.Add(i++, row[0]);
        }

        return classMap;
    }
}