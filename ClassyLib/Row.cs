using System.Collections;
using System.Text;

namespace ClassyLib;

/// <summary>
/// Generic data row
/// </summary>
/// <typeparam name="T"></typeparam>
public class Row<T> : IEnumerable
{
    private List<T> data = new List<T>();

    public T this[short i]
    {
        get => data[i];
        set => data[i] = value;
    }
    
    public int Count => data.Count;

    /// <summary>
    /// Append a new cell to the row
    /// </summary>
    /// <param name="item">cell type</param>
    public Row<T> Append(T item)
    {
        data.Add(item);
        return this;
    }
    
    /// <summary>
    /// Append many cells to the row
    /// </summary>
    /// <param name="items">cell type</param>
    public Row<T> Append(T[] items)
    {
        data.AddRange(items);
        return this;
    }

    public IEnumerator GetEnumerator()
    {
        return data.GetEnumerator();
    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        foreach (var item in data)
        {
            sb.Append(item);
            sb.Append(',');
        }
        return sb.ToString();
    }
}