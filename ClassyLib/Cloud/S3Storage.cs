using System.Net;
using Amazon.S3;
using Amazon.S3.Model;
using ClassyLib.IO;
using ClassyLib.Network;

namespace ClassyLib.Cloud;

public class S3Storage
{
    private readonly IProgressBar _progressBar;
    private readonly SpeedTracker _speedTracker;
    private readonly CsvFileReader _fileReader;
    private readonly ILogger _logger;
    private const int UploadCheckIntervalMilliseconds = 50;

    public S3Storage(
        IProgressBar progressBar, 
        SpeedTracker speedTracker, 
        CsvFileReader fileReader,
        ILogger logger)
    {
        _fileReader = fileReader;
        _progressBar = progressBar;
        _speedTracker = speedTracker;
        _logger = logger;
    }
    
    /// <summary>
    /// Upload file to specific S3 location
    /// </summary>
    /// <param name="filePath">Local file path</param>
    /// <param name="bucketName">S3 bucket name</param>
    /// <param name="bucketDirectoryName">S3 bucket path</param>
    /// <returns></returns>
    public virtual async Task Upload(string filePath, string bucketName, string bucketDirectoryName)
    {
        if (filePath == null) throw new ArgumentNullException(nameof(filePath));
        if (bucketName == null) throw new ArgumentNullException(nameof(bucketName));
        if (bucketDirectoryName == null) throw new ArgumentNullException(nameof(bucketDirectoryName));

        using AmazonS3Client client = new AmazonS3Client();
        var request = new PutObjectRequest()
        {
            BucketName = bucketName,
            Key = $"{bucketDirectoryName}/{filePath?.LastSplit('/')}",
            FilePath = filePath
        };
        Task<PutObjectResponse> response = client.PutObjectAsync(request);
        var done = false;
        var totalBytes = CsvFileReader.GetFileSizeBytes(filePath!);
        double bytesUploaded = 0;
        var bytesPerInterval = await _speedTracker.GetDownloadBitSpeedPerSecond() / 1000 * UploadCheckIntervalMilliseconds;
        _logger.Info("Uploading dataset to S3...");
        while (!done)
        {
            _progressBar.WritePercentComplete((short)(100*bytesUploaded/totalBytes));
            bytesUploaded += bytesPerInterval;
            if (response.IsCompleted)
            {
                done = true;
                if (response.Result.HttpStatusCode == HttpStatusCode.OK)
                {
                    _progressBar.WritePercentComplete(100);
                    _progressBar.Stop();
                    _logger.Info("Done");
                }
                else
                {
                    _logger.Info($"Could not upload {request.Key} to {request.BucketName}/{request.FilePath}");
                }
            }
            await Task.Delay(TimeSpan.FromMilliseconds(UploadCheckIntervalMilliseconds));
        }
    }
}