namespace ClassyLib.Cloud;

public class InferenceRequestModel
{
    public IEnumerable<string>? Instances { get; set; }
}