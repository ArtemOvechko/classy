namespace ClassyLib.Cloud;

public class InferenceResponseModel
{
    public string[]? Label { get; set; }
    
    public double[]? Prob { get; set; }
}