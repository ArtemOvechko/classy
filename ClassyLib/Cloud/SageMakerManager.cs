using System.Text;
using Amazon.SageMaker;
using Amazon.SageMaker.Model;
using Amazon.SageMakerRuntime;
using Amazon.SageMakerRuntime.Model;
using ClassyLib.IO;
using ClassyLib.Preprocessing;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ClassyLib.Cloud;

/// <summary>
/// Used to train, deploy and use text classification models in AWS SageMaker
/// </summary>
public class SageMakerManager
{
    private readonly IProgressBar _progressBar;
    private readonly RowTokenizer _rowTokenizer;
    private readonly ILogger _logger;

    private const short JobStatusCheckIntervalSeconds = 6;

    public SageMakerManager(
        IProgressBar progressBar,
        RowTokenizer rowTokenizer,
        ILogger logger)
    {
        _progressBar = progressBar;
        _rowTokenizer = rowTokenizer;
        _logger = logger;
    }

    /// <summary>
    /// Trains a model and then deploys it using data set artifacts in S3
    /// </summary>
    /// <param name="s3TrainingDataPath"></param>
    /// <param name="s3ValidationDataPath"></param>
    /// <param name="s3OutputDataPath"></param>
    /// <param name="iamRoleNameForSetup"></param>
    public virtual async Task PrepareModelAsync(
        string s3TrainingDataPath, 
        string s3ValidationDataPath, 
        string s3OutputDataPath,
        string iamRoleNameForSetup)
    {
        var modelPath = await TrainModelAsync(
            s3TrainingDataPath, s3ValidationDataPath, s3OutputDataPath, iamRoleNameForSetup);
        await CreateModelAsync(modelPath, iamRoleNameForSetup);
    }
    
    private async Task<string> TrainModelAsync(
        string s3TrainingDataPath, string s3ValidationDataPath, string s3OutputDataPath, string roleArn)
    {
        using var sageMakerClient = new AmazonSageMakerClient();

        var trainingJobRequest = new CreateTrainingJobRequest()
        {
            AlgorithmSpecification = new AlgorithmSpecification()
            {
                TrainingImage = "811284229777.dkr.ecr.us-east-1.amazonaws.com/blazingtext:1",
                TrainingInputMode = TrainingInputMode.File
            },
            TrainingJobName = $"classy-job-{DateTime.UtcNow.Ticks}",
            HyperParameters = new Dictionary<string, string>()
            {
                { "mode", "supervised" },
                { "epochs", "1" },
                { "min_count", "2" },
                { "learning_rate", "0.05" },
                { "vector_dim", "10" },
                { "early_stopping", "True" },
                { "patience", "4" },
                { "min_epochs", "5" },
                { "word_ngrams", "2" }
            },
            RoleArn = roleArn,
            InputDataConfig = new List<Channel>()
            {
                new Channel()
                {
                    ChannelName = "train",
                    DataSource = new DataSource()
                    {
                        S3DataSource = new S3DataSource()
                        {
                            S3Uri = s3TrainingDataPath,
                            S3DataDistributionType = S3DataDistribution.FullyReplicated,
                            S3DataType = S3DataType.S3Prefix
                        }
                    },
                    ContentType = "text/plain"
                },
                new Channel()
                {
                    ChannelName = "validation",
                    DataSource = new DataSource()
                    {
                        S3DataSource = new S3DataSource()
                        {
                            S3Uri = s3ValidationDataPath,
                            S3DataDistributionType = S3DataDistribution.FullyReplicated,
                            S3DataType = S3DataType.S3Prefix
                        }
                    },
                    ContentType = "text/plain"
                },
            },
            OutputDataConfig = new OutputDataConfig()
            {
                S3OutputPath = s3OutputDataPath
            },
            ResourceConfig = new ResourceConfig()
            {
                InstanceType = new TrainingInstanceType(InstanceType.MlC44xlarge),
                InstanceCount = 1,
                VolumeSizeInGB = 30
            },
            StoppingCondition = new StoppingCondition()
            {
                MaxRuntimeInSeconds = 360
            }
        };
        
        try
        {
            await sageMakerClient.CreateTrainingJobAsync(trainingJobRequest);
            var waitingOnJob = true;
            _logger.Info("Starting training job...");
            var awaiterStarted = false;
            while (waitingOnJob)
            {
                var response = await sageMakerClient.DescribeTrainingJobAsync(new DescribeTrainingJobRequest()
                {
                    TrainingJobName = trainingJobRequest.TrainingJobName
                });
                if (!awaiterStarted)
                {
                    _logger.Info("The job has been started. Waiting on completion...");
                    awaiterStarted = true;
                    _progressBar.StartAwaiter();
                }

                if (response.TrainingJobStatus != TrainingJobStatus.InProgress)
                {
                    _progressBar.Stop();
                    waitingOnJob = false;
                }
                if (response.TrainingJobStatus == TrainingJobStatus.Completed)
                {
                    _logger.Info("The job has been completed.");
                    return response.ModelArtifacts.S3ModelArtifacts;
                }
                if (response.TrainingJobStatus == TrainingJobStatus.Stopped 
                    ||  response.TrainingJobStatus == TrainingJobStatus.Stopping)
                {
                    throw new Exception("The job has been stopped.");
                }
                if (response.TrainingJobStatus == TrainingJobStatus.Failed)
                {
                    throw new Exception($"The job has been failed. Reason: {response.FailureReason}");
                }
                await Task.Delay(TimeSpan.FromSeconds(JobStatusCheckIntervalSeconds));
            }

            throw new Exception("Unrecognized training job error occured.");
        }
        catch (Exception)
        {
            _progressBar.Stop();
            throw;
        }
    }

    /// <summary>
    /// Creates an endpoint config and assigns it to a new endpoint
    /// </summary>
    /// <param name="modelName"></param>
    public virtual async Task SetupEndpointAsync(string modelName)
    {
        var endpointConfigName = await CreateEndpointConfigAsync(modelName);
        await CreateEndpointAsync(endpointConfigName);
    }

    private async Task CreateModelAsync(string s3SourcePath, string roleArn)
    {
        using var sageMakerClient = new AmazonSageMakerClient();
        var createModelRequest = new CreateModelRequest()
        {
            Containers = new List<ContainerDefinition>()
            {
                new ContainerDefinition()
                {
                    Image = "811284229777.dkr.ecr.us-east-1.amazonaws.com/blazingtext:1",
                    Mode = ContainerMode.SingleModel,
                    ModelDataUrl = s3SourcePath
                }
            },
            ExecutionRoleArn = roleArn,
            ModelName = $"classy-model-{DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()}",
            
        };
        _logger.Info("Deploying model...");
        _progressBar.StartAwaiter();
        try
        {
            await sageMakerClient.CreateModelAsync(createModelRequest);
            _progressBar.Stop();
            _logger.Info($"Model has been deployed. Model name: {createModelRequest.ModelName}");
        }
        catch (Exception)
        {
            _progressBar.Stop();
            throw;
        }
    }

    private async Task<string> CreateEndpointConfigAsync(string modelName)
    {
        using var sageMakerClient = new AmazonSageMakerClient();
        var endpointConfigRequest = new CreateEndpointConfigRequest
        {
            EndpointConfigName = $"classy-{DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()}",
            ProductionVariants = new List<ProductionVariant>()
            {
                new ProductionVariant()
                {
                    InitialInstanceCount = 1,
                    InstanceType = ProductionVariantInstanceType.MlM4Xlarge,
                    ModelName = modelName,
                    VariantName = "Production"
                }
            }
        };
        _logger.Info("Creating endpoint config...");
        await sageMakerClient.CreateEndpointConfigAsync(endpointConfigRequest);
        _progressBar.Stop();
        _logger.Info("Endpoint config created.");
        
        return endpointConfigRequest.EndpointConfigName;
    }

    private async Task CreateEndpointAsync(string endpointConfigName)
    {
        using var sageMakerClient = new AmazonSageMakerClient();
        var createEndpointRequest = new CreateEndpointRequest()
        {
            EndpointConfigName = endpointConfigName,
            EndpointName = $"classy-{DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()}"
        };
        
        _logger.Info("Creating endpoint...");
        _progressBar.StartAwaiter();
        var waitingOnJob = true;
        try
        {
            await sageMakerClient.CreateEndpointAsync(createEndpointRequest);
            while (waitingOnJob)
            {
                var describeEndpointRequest = new DescribeEndpointRequest()
                {
                    EndpointName = createEndpointRequest.EndpointName
                };
                var describeEndpointResponse = 
                    await sageMakerClient.DescribeEndpointAsync(describeEndpointRequest);
                if (describeEndpointResponse.EndpointStatus == EndpointStatus.Creating)
                {
                    continue;
                }
                
                if (describeEndpointResponse.EndpointStatus != EndpointStatus.Creating)
                {
                    _progressBar.Stop();
                    waitingOnJob = false;
                    _logger.Info($"Endpoint name: {createEndpointRequest.EndpointName}");
                }
                
                if (describeEndpointResponse.EndpointStatus == EndpointStatus.InService)
                {
                    _logger.Info($"Endpoint set up.");
                }
                
                if (describeEndpointResponse.EndpointStatus == EndpointStatus.Failed)
                {
                    _logger.Info($"Endpoint creation failed. Reason: {describeEndpointResponse.FailureReason}");
                }
                
                if (describeEndpointResponse.EndpointStatus != EndpointStatus.InService && 
                    describeEndpointResponse.EndpointStatus != EndpointStatus.Failed)
                {
                    _logger.Info($"Endpoint setup may not have finished normally. Endpoint status: {describeEndpointResponse.EndpointStatus}");
                }
            }
        }
        catch (Exception)
        {
            _progressBar.Stop();
            throw;
        }
    }

    /// <summary>
    /// Produce text classification inference on the endpoint
    /// </summary>
    /// <param name="inputText"></param>
    /// <param name="endpointName"></param>
    public virtual async Task MakeInferenceAsync(string inputText, string endpointName)
    {
        using var sageMakerRuntimeClient = new AmazonSageMakerRuntimeClient();
        var row = new Row<string>().Append(inputText);
        var tokenizedRow = _rowTokenizer.Tokenize(row);
        var inputTokenized = new StringBuilder().BuildFrom(tokenizedRow, ' ').ToString();
        var payload = new InferenceRequestModel
        {
            Instances = new[] { inputTokenized }
        };
        using var ms = new MemoryStream(
            Encoding.UTF8.GetBytes(
                JsonConvert.SerializeObject(payload, new JsonSerializerSettings()
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                })));
        var invokeEndpointRequest = new InvokeEndpointRequest()
        {
            EndpointName = endpointName,
            ContentType = "application/json",
            Body = ms
        };
        
        _progressBar.StartAwaiter(true);
        _logger.Info("Classifying...");
        try
        {
            var response = 
                await sageMakerRuntimeClient.InvokeEndpointAsync(invokeEndpointRequest);
            using var streamReader = new StreamReader(response.Body);
            var inference = await streamReader.ReadToEndAsync();
            _progressBar.Stop();
            var inferenceModel = JsonConvert.DeserializeObject<InferenceResponseModel[]>(inference, new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
            _logger.Info($"This is a {inferenceModel!.First().Label!.First().ParseLabel()} text");
        }
        catch (Exception)
        {
            _progressBar.Stop();
            throw;
        }
    }
}