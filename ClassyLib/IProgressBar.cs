namespace ClassyLib;

public interface IProgressBar
{
    void StartAwaiter(bool fancy = false);

    void Stop();

    void WritePercentComplete(short value);

    void HandleExit();
}