using System.Diagnostics;
using System.Net;

namespace ClassyLib.Network;

public class SpeedTracker
{
    public virtual Task<double> GetDownloadBitSpeedPerSecond()
    {
        var stopwatch = new Stopwatch();
            
        stopwatch.Reset();
        stopwatch.Start();

#pragma warning disable SYSLIB0014
        WebClient webClient = new WebClient();
#pragma warning restore SYSLIB0014
        var bytes = webClient.DownloadData("http://www.google.com");

        stopwatch.Stop();

        var seconds = stopwatch.Elapsed.TotalSeconds;

        var speed = (bytes.Count() * 8) / seconds;

        return Task.FromResult(speed);
    }
}