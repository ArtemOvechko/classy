using ClassyLib.IO;

namespace ClassyLib.Preprocessing;

public class Preprocessor
{
    private readonly CsvFileReader _fileReader;
    private readonly RowTokenizer _rowTokenizer;
    private readonly CsvFileWriter _fileWriter;
    private readonly DataClassMapper _dataClassMapper;
    private readonly IProgressBar _progressBar;
    private readonly ILogger _logger;

    private const char CsvReadSeparator = ',';
    private const char CsvWriteSeparator = ' ';

    public Preprocessor(
        CsvFileReader fileReader,
        CsvFileWriter fileWriter,
        DataClassMapper dataClassMapper,
        IProgressBar progressBar,
        RowTokenizer rowTokenizer,
        ILogger logger)
    {
        _fileReader = fileReader;
        _fileWriter = fileWriter;
        _dataClassMapper = dataClassMapper;
        _rowTokenizer = rowTokenizer;
        _progressBar = progressBar;
        _logger = logger;
    }
    
    /// <summary>
    /// Prepare data set
    /// </summary>
    /// <param name="dataSetPath">Raw data set file path</param>
    /// <param name="dataClassesPath">File with data classes</param>
    /// <param name="outputFileName">Prepared data set file name</param>
    /// <param name="dataPercentage">Percentage of random rows to take from the initial data set.</param>
    public async Task PreprocessAsync(
        string dataSetPath, string dataClassesPath, string outputFileName, float dataPercentage = 1)
    {
        if (dataSetPath == null) throw new ArgumentNullException(nameof(dataSetPath));
        var rows = await _fileReader.ReadAsync(dataSetPath, CsvReadSeparator);
        rows.Shuffle();
        if (dataPercentage <= 0 || dataPercentage > 1)
        {
            throw new ArgumentException("Data percentage should be greater than 0 and equal or lower than 1", nameof(dataPercentage));
        }
        rows = rows.Take((int)(dataPercentage! * rows.Count)).ToList();
        rows = await _dataClassMapper.MapTo(rows, dataClassesPath);
        _logger.Info("Tokenizing input data...");
        for (var i = 0; i < rows.Count; i++)
        {
            rows[i] = _rowTokenizer.Tokenize(rows[i]);
            _progressBar.WritePercentComplete((short)(((float)(i+1)/rows.Count)*100));
        }
        _progressBar.WritePercentComplete(100);
        _progressBar.Stop();
        _logger.Info("Done.");
        await _fileWriter.WriteAsync(outputFileName, rows, CsvWriteSeparator);
        _logger.Info("Preprocessing complete.");
    }
}
