namespace ClassyLib.Preprocessing;

/// <summary>
/// Classy.Row tokenizer for Blazing Text classification
/// </summary>
public class RowTokenizer
{
    private const char Separator = ' ';
    
    /// <summary>
    /// Tokenize row
    /// </summary>
    /// <param name="row">a row to tokenize</param>
    /// <returns>New tokenized row</returns>
    public Row<string> Tokenize(Row<string> row)
    {
        var tokenizedRow = new Row<string>();
        short i = 0;
        if (row.Count > 1)
        {
            tokenizedRow.Append($"__label__{row[i++].ToString()}");
        }
        while (i < row.Count)
        {
            tokenizedRow.Append(row[i++].ToLowerInvariant().Split(
                new char[]
                {
                    ' ',
                    ',',
                    '.',
                    ';',
                    '?',
                    '!',
                    ':',
                    '%',
                    '$',
                    '&'
                }, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries));
        }
        return tokenizedRow;
    }
}