using Newtonsoft.Json;

namespace ClassyLib.Settings;

public class SettingsManager
{
    public Settings? Settings { get; private set; }
    private const string SettingsFileName = "settings.classy";
    private readonly string Path;
    
    public SettingsManager()
    {
        Path = $"{Environment.CurrentDirectory}/{SettingsFileName}";
    }

    public async Task Init()
    {
        if (!File.Exists(Path))
        {
            throw new Exception("There is no settings.classy file. Use classy configure command to create one.");
        }
        var content = await File.ReadAllTextAsync(Path);
        Settings = JsonConvert.DeserializeObject<Settings>(content)!;
    }

    public async Task Configure(Settings newSettings)
    {
        // Overwrite empty new settings with existing ones
        if (File.Exists(Path))
        {
            var content = await File.ReadAllTextAsync(Path);
            var currentSettings = JsonConvert.DeserializeObject<Settings>(content)!;
            var newInferenceEndpointName = 
                string.IsNullOrWhiteSpace(newSettings.InferenceEndpointName)
                    ? currentSettings.InferenceEndpointName
                    : newSettings.InferenceEndpointName;
            var newIAMRoleName = 
                string.IsNullOrWhiteSpace(newSettings.IAMRoleNameForSetup)
                    ? currentSettings.IAMRoleNameForSetup
                    : newSettings.IAMRoleNameForSetup;
            newSettings = new Settings(newInferenceEndpointName, newIAMRoleName);
        }
        await File.WriteAllTextAsync(
            Path, JsonConvert.SerializeObject(newSettings, Formatting.Indented)!);
    }
}