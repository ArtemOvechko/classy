using Newtonsoft.Json;

namespace ClassyLib.Settings;

public class Settings
{
    [JsonProperty]
    public readonly string? InferenceEndpointName;
    
    [JsonProperty]
    public readonly string? IAMRoleNameForSetup;

    public Settings(string? inferenceEndpointName, string? iAMRoleNameForSetup)
    {
        InferenceEndpointName = inferenceEndpointName;
        IAMRoleNameForSetup = iAMRoleNameForSetup;
    }
}