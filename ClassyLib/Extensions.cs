using System.Text;
using ClassyLib.Cloud;
using ClassyLib.IO;
using ClassyLib.Network;
using ClassyLib.Preprocessing;
using ClassyLib.Settings;
using Microsoft.Extensions.DependencyInjection;

namespace ClassyLib;

public static class Extensions
{
    private static readonly Random Rand = new Random();
    
    public static void Shuffle<T>(this IList<T> list)
    {
        for (var i = list.Count - 1; i > 0; i--)
        {
            var n = Rand.Next(i + 1);
            (list[i], list[n]) = (list[n], list[i]);
        }
    }

    public static string LastSplit(this string str, char separator)
    {
        var parts = str.Split(separator);
        return parts[^1];
    }

    /// <summary>
    /// Reuse a StringBuilder instance to build a Classy.row 
    /// </summary>
    /// <param name="stringBuilder">Instance to reuse</param>
    /// <param name="row">Row to stringify</param>
    /// <param name="separator">.csv separator to use</param>
    /// <typeparam name="T">Row data type</typeparam>
    public static StringBuilder BuildFrom<T>(this StringBuilder stringBuilder, Row<T> row, char separator)
    {
        stringBuilder.Clear();
        for (short i = 0; i < row.Count; i++)
        {
            stringBuilder.Append(row[i]);
            if (i < row.Count - 1)
            {
                stringBuilder.Append(separator);
            }
        }

        return stringBuilder;
    }

    public static string ParseLabel(this string sourceString)
    {
        return sourceString.Replace("__label__", string.Empty);
    }

    public static IServiceCollection AddClassy<TProgressBar, TLogger>
        (this IServiceCollection services) 
        where TProgressBar : class, IProgressBar
        where TLogger : class, ILogger
    {
        return services
            .AddSingleton<ClassyApp>()
            .AddSingleton<CsvFileReader>()
            .AddSingleton<Preprocessor>()
            .AddSingleton<SettingsManager>()
            .AddSingleton<IProgressBar, TProgressBar>()
            .AddSingleton<CsvFileReader>()
            .AddSingleton<CsvFileWriter>()
            .AddSingleton<SpeedTracker>()
            .AddSingleton<RowTokenizer>()
            .AddSingleton<S3Storage>()
            .AddSingleton<SageMakerManager>()
            .AddSingleton<DataClassMapper>()
            .AddSingleton<ILogger, TLogger>();
    }
}