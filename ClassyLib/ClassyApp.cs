using ClassyLib.Cloud;
using ClassyLib.Preprocessing;
using ClassyLib.Settings;

namespace ClassyLib;

public class ClassyApp
{
    public readonly Preprocessor Preprocessor;
    public readonly S3Storage S3Storage;
    public readonly SageMakerManager SageMakerManager;
    public readonly SettingsManager SettingsManager;
    private readonly IProgressBar _progressBar;

    public ClassyApp(
        Preprocessor preprocessor,
        S3Storage s3Storage,
        SageMakerManager sageMakerManager,
        SettingsManager settingsManager,
        IProgressBar progressBar
        )
    {
        Preprocessor = preprocessor;
        S3Storage = s3Storage;
        SageMakerManager = sageMakerManager;
        SettingsManager = settingsManager;
        _progressBar = progressBar;
    }
    
    public void Exit(int exitCode)
    {
        _progressBar.HandleExit();
        Environment.Exit(exitCode);
    }
}