namespace ClassyLib.IO;

/// <summary>
/// Reads csv as rows list
/// </summary>
public class CsvFileReader
{
    private const int RowCount = 561000;
    private readonly IProgressBar _progressBar;
    private readonly ILogger _logger;
    
    public CsvFileReader(IProgressBar progressBar, ILogger logger)
    {
        _progressBar = progressBar;
        _logger = logger;
    }

    public static long GetFileSizeBytes(string path)
    {
        if (path == null) throw new ArgumentNullException(nameof(path));
        
        var fileInfo = new FileInfo(path);
        var totalBytes = fileInfo.Length;
        
        return totalBytes;
    }

    /// <summary>
    /// Read .csv using specific separator
    /// </summary>
    /// <param name="path">.csv file path</param>
    /// <param name="separator">column separator</param>
    /// <returns></returns>
    public virtual async Task<List<Row<string>>> ReadAsync(string path, char separator)
    {
        var rows = new List<Row<string>>(RowCount);
        using var reader = new StreamReader(path);
        _logger.Info($"Reading csv file ({path})...");
        var totalBytes = GetFileSizeBytes(path);
        double currentBytes = 0f;
        while (!reader.EndOfStream)
        {
            var line = await reader.ReadLineAsync();
            var lineSize = System.Text.Encoding.UTF8.GetByteCount(line!);
            var values = line!.Replace("\"", "").Split(separator);
            var newRow = new Row<string>();
            foreach (var value in values)
            {
                newRow.Append(value);
            }
            rows.Add(newRow);
            currentBytes += lineSize;
            _progressBar.WritePercentComplete((short)((currentBytes/totalBytes)*100));
        }
        _progressBar.WritePercentComplete(100);
        _progressBar.Stop();
        _logger.Info("Done.");

        return rows;
    }
}