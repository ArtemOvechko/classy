using System.Text;

namespace ClassyLib.IO;

/// <summary>
/// Writes rows list to .csv
/// </summary>
public class CsvFileWriter
{
    private const string LineSeparator = "\n";
    private readonly IProgressBar _progressBar;
    private readonly ILogger _logger;

    public CsvFileWriter(
        IProgressBar progressBar,
        ILogger logger)
    {
        _progressBar = progressBar;
        _logger = logger;
    }

    /// <summary>
    /// Write to a .csv file
    /// </summary>
    /// <param name="fileName">Destination file name. The path is app's directory</param>
    /// <param name="dataRows">Rows to write</param>
    /// <param name="separator">Column separator</param>
    /// <typeparam name="T">Custom row type to map. Underlying type should implement ToString() method</typeparam>
    public virtual async Task WriteAsync<T>(string fileName, List<Row<T>> dataRows, char separator)
    {
        var stringBuilder = new StringBuilder();
        var path = $"{Environment.CurrentDirectory}/{fileName}";
        _logger.Info($"Writing processed dataset file ({path})...");
        await using var streamWriter = File.CreateText(path);
        var i = 0f;
        foreach (var row in dataRows)
        {
            stringBuilder.BuildFrom(row, separator);
            await streamWriter.WriteLineAsync(stringBuilder.ToString());
            _progressBar.WritePercentComplete((short)((i++/dataRows.Count)*100));
        }
        _progressBar.WritePercentComplete(100);
        _progressBar.Stop();
        _logger.Info("Done.");
    }
}