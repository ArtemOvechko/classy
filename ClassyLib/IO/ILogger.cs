namespace ClassyLib.IO;

public interface ILogger
{
    void Info(string data);
}